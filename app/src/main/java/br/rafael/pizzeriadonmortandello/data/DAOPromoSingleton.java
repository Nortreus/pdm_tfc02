package br.rafael.pizzeriadonmortandello.data;

import java.util.ArrayList;

import br.rafael.pizzeriadonmortandello.model.Promocoes;

public class DAOPromoSingleton {

    private static DAOPromoSingleton INSTANCE;
    private ArrayList<Promocoes> promos;

    private DAOPromoSingleton(){
        this.promos = new ArrayList<>();
    }

    public static DAOPromoSingleton getINSTANCE(){
        if(INSTANCE == null){
            INSTANCE = new DAOPromoSingleton();
        }
        return INSTANCE;
    }

    public ArrayList<Promocoes> getPromos(){
        return this.promos;
    }

    public void addPromo(Promocoes promo){
        this.promos.add(promo);
    }
}
