package br.rafael.pizzeriadonmortandello.data;

import java.util.ArrayList;

import br.rafael.pizzeriadonmortandello.model.Pizza;
import br.rafael.pizzeriadonmortandello.model.Promocoes;

public class DummyDataPromo {
    private int numPizzas;
    private double pctDesc;
    private Promocoes promocao;
    private int indice = 0;
    public ArrayList<Pizza> pizzas = DAOPizzaSingleton.getINSTANCE().getPizzas();

    public DummyDataPromo(){
        for(int i=0; i<5; i++){
            if(!pizzas.isEmpty()){
                ArrayList<Pizza> pizzasPromotion = new ArrayList<>();
                numPizzas = (int) Math.ceil(Math.random() * (3 - 1) + 1);
                pctDesc = Math.random() * (20 - 5) + 5;
                promocao = new Promocoes("Promo"+(i+1),pctDesc,pizzas);



                for(int j=0; j<numPizzas; j++){
                    indice = (int) Math.ceil(Math.random() * (pizzas.size() - 0));
                    if(pizzas.get(indice-1) != null){
                        pizzasPromotion.add(pizzas.get(indice-1));
                    }
                }
                promocao.setPizzas(pizzasPromotion);
                DAOPromoSingleton.getINSTANCE().addPromo(promocao);
            }
        }
    }

}
