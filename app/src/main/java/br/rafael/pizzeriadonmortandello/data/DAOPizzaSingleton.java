package br.rafael.pizzeriadonmortandello.data;

import java.util.ArrayList;

import br.rafael.pizzeriadonmortandello.model.Pizza;

public class DAOPizzaSingleton {

    private static DAOPizzaSingleton INSTANCE;
    private ArrayList<Pizza> pizzas;

    private DAOPizzaSingleton(){
        this.pizzas = new ArrayList<>();
    }

    public static DAOPizzaSingleton getINSTANCE(){
        if(INSTANCE == null){
            INSTANCE = new DAOPizzaSingleton();
        }
        return INSTANCE;
    }

    public ArrayList<Pizza> getPizzas(){
        return this.pizzas;
    }

    public void addPizza(Pizza pizza){
        this.pizzas.add(pizza);
    }


}
