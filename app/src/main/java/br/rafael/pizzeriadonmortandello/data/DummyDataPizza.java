package br.rafael.pizzeriadonmortandello.data;

import br.rafael.pizzeriadonmortandello.model.Pizza;

public class DummyDataPizza {

    public DummyDataPizza(){
        DAOPizzaSingleton.getINSTANCE().addPizza(new Pizza("Calabresa",37.5,"calabresa, queijo, cebola"));
        DAOPizzaSingleton.getINSTANCE().addPizza(new Pizza("Frango",40,"frango, queijo, molho de tomate"));
        DAOPizzaSingleton.getINSTANCE().addPizza(new Pizza("4 Queijos",35,"queijo prata, mussarela, cheedar"));

    }

}
