package br.rafael.pizzeriadonmortandello.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;

import br.rafael.pizzeriadonmortandello.R;
import br.rafael.pizzeriadonmortandello.data.DAOPromoSingleton;
import br.rafael.pizzeriadonmortandello.data.DummyDataPromo;
import br.rafael.pizzeriadonmortandello.ui.list.promo.PromoDataAdapter;
import br.rafael.pizzeriadonmortandello.ui.list.promo.PromoDataViewHolder;

public class PromoMenuActivity extends AppCompatActivity {

    private RecyclerView rvPromo;
    private LinearLayoutManager layoutManager;
    private PromoDataAdapter promoDataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_menu);
        this.rvPromo = findViewById(R.id.rvPromos);
        this.layoutManager = new LinearLayoutManager(this);
        this.promoDataAdapter = new PromoDataAdapter(DAOPromoSingleton.getINSTANCE().getPromos());
        this.rvPromo.setAdapter(this.promoDataAdapter);
        this.rvPromo.setLayoutManager(this.layoutManager);

        if(DAOPromoSingleton.getINSTANCE().getPromos().size()==0){
            new DummyDataPromo();
        }

    }
}