package br.rafael.pizzeriadonmortandello.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.rafael.pizzeriadonmortandello.R;
import br.rafael.pizzeriadonmortandello.data.DAOPizzaSingleton;
import br.rafael.pizzeriadonmortandello.model.Pizza;
import br.rafael.pizzeriadonmortandello.ui.dialogs.DialogPizza;
import br.rafael.pizzeriadonmortandello.ui.list.pizza.PizzaDataAdapter;

public class PizzaMenuActivity extends AppCompatActivity {



    private RecyclerView rvPizzas;
    private LinearLayoutManager layoutManager;
    private PizzaDataAdapter pizzaDataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_menu);
        this.rvPizzas = findViewById(R.id.rvPizzas);
        this.layoutManager = new LinearLayoutManager(this);
        this.pizzaDataAdapter = new PizzaDataAdapter(DAOPizzaSingleton.getINSTANCE().getPizzas());
        this.rvPizzas.setLayoutManager(this.layoutManager);
        this.rvPizzas.setAdapter(this.pizzaDataAdapter);


        
    }








}