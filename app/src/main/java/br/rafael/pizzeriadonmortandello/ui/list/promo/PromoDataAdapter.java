package br.rafael.pizzeriadonmortandello.ui.list.promo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.rafael.pizzeriadonmortandello.R;
import br.rafael.pizzeriadonmortandello.model.Promocoes;

public class PromoDataAdapter extends RecyclerView.Adapter<PromoDataViewHolder> {

    private ArrayList<Promocoes> promos;

    public PromoDataAdapter(ArrayList<Promocoes> promos) {
        this.promos = promos;
    }

    @NonNull
    @Override
    public PromoDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.promo_menu_item,parent,false);
        PromoDataViewHolder holder = new PromoDataViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PromoDataViewHolder holder, int position) {
        holder.bind(this.promos.get(position));
    }

    @Override
    public int getItemCount() {
        return this.promos.size();
    }

    @Override
    public int getItemViewType(int position){
        return super.getItemViewType(position);
    }
}
