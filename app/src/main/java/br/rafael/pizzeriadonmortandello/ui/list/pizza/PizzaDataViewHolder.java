package br.rafael.pizzeriadonmortandello.ui.list.pizza;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.rafael.pizzeriadonmortandello.R;
import br.rafael.pizzeriadonmortandello.model.Pizza;
import br.rafael.pizzeriadonmortandello.ui.dialogs.DialogPizza;

public class PizzaDataViewHolder extends RecyclerView.ViewHolder {

    private final TextView pNome;
    private final Button pPreco;
    private final TextView pDesc;
    private Pizza pizzaAtual;

    public PizzaDataViewHolder(@NonNull View itemView) {
        super(itemView);
        this.pNome = itemView.findViewById(R.id.pizzaNome);
        this.pPreco = itemView.findViewById(R.id.pizzaPreco);
        this.pDesc = itemView.findViewById(R.id.pizzaDesc);
        this.pDesc.setVisibility(View.GONE);
        itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view1){
                if(pDesc.getVisibility() == View.GONE){
                    pDesc.setVisibility(View.VISIBLE);
                }else{
                    pDesc.setVisibility(View.GONE);
                }
            }
        });

    }

    public void bind(Pizza pizza){
        this.pNome.setText(pizza.getNome());
        this.pPreco.setText(pizza.getPreco().toString());
        this.pDesc.setText(pizza.getDescricao());
        this.pizzaAtual = pizza;

        this.pPreco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DialogPizza(pizza, v.getContext());
            }
        });
    }
}
