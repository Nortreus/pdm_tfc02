package br.rafael.pizzeriadonmortandello.ui.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import org.jetbrains.annotations.NotNull;

import br.rafael.pizzeriadonmortandello.R;
import br.rafael.pizzeriadonmortandello.model.Pizza;

public  class DialogPizza {

    public DialogPizza(@NotNull Pizza pizza, Context context){
        AlertDialog.Builder build =  new AlertDialog.Builder(context);

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_pizza,null);

        TextView pizzaNome = view.findViewById(R.id.pizzaNome);
        pizzaNome.setText(pizza.getNome());
        TextView pizzaDescricao = view.findViewById(R.id.pizzaDesc);
        pizzaDescricao.setText(pizza.getDescricao());
        TextView pequena = view.findViewById(R.id.pequena);
        TextView media = view.findViewById(R.id.media);
        TextView grande = view.findViewById(R.id.grande);
        pequena.setText(pizza.getPrecoPequena().toString());
        media.setText(pizza.getPreco().toString());
        grande.setText(pizza.getPrecoGrande().toString());
        Button btn = view.findViewById(R.id.okay);

        build.setTitle("Informações:");
        build.setView(view);
        AlertDialog delete = build.create();
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                delete.dismiss();
            }
        });

        delete.show();

    }
}
