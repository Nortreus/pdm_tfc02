package br.rafael.pizzeriadonmortandello.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import br.rafael.pizzeriadonmortandello.R;
import br.rafael.pizzeriadonmortandello.data.DAOPizzaSingleton;
import br.rafael.pizzeriadonmortandello.data.DAOPromoSingleton;
import br.rafael.pizzeriadonmortandello.data.DummyDataPizza;
import br.rafael.pizzeriadonmortandello.data.DummyDataPromo;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(DAOPizzaSingleton.getINSTANCE().getPizzas().size() == 0){
            new DummyDataPizza();
        }


    }

    public void onClickCadPizza(View view){
        Intent openCadPizza = new Intent(this, NewPizzaActivity.class);
        startActivity(openCadPizza);
    }

    public void onClickMenuPizza(View view){
        Intent openMenuPizza = new Intent(this, PizzaMenuActivity.class);
        startActivity(openMenuPizza);
    }

    public void onClickMenuPromo(View view){
        Intent openMenuPromo =  new Intent(this,PromoMenuActivity.class);
        startActivity(openMenuPromo);
    }




}