package br.rafael.pizzeriadonmortandello.ui.list.promo;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.rafael.pizzeriadonmortandello.R;
import br.rafael.pizzeriadonmortandello.model.Pizza;
import br.rafael.pizzeriadonmortandello.model.Promocoes;
import br.rafael.pizzeriadonmortandello.ui.dialogs.DialogPizza;
import br.rafael.pizzeriadonmortandello.ui.dialogs.DialogPromo;

public class PromoDataViewHolder extends RecyclerView.ViewHolder {

    private TextView promoName;
    private ImageButton promoButton;
    private TextView promoDesc;
    private Promocoes promoAtual;

    public PromoDataViewHolder(@NonNull View itemView) {
        super(itemView);
        this.promoName = itemView.findViewById(R.id.promoName);
        this.promoButton = itemView.findViewById(R.id.promoButton);
        this.promoDesc = itemView.findViewById(R.id.promoDesc);
        this.promoDesc.setVisibility(View.GONE);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                if (promoDesc.getVisibility() == View.GONE) {
                    promoDesc.setVisibility(View.VISIBLE);
                } else {
                    promoDesc.setVisibility(View.GONE);
                }
            }
        });
    }

    public void bind(Promocoes promo){
        this.promoName.setText(promo.getNome());
        this.promoDesc.setText(promo.mostraPromoçes());
        this.promoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DialogPromo(promo, v.getContext());
            }
        });


    }
}
