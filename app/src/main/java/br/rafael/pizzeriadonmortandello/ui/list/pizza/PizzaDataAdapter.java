package br.rafael.pizzeriadonmortandello.ui.list.pizza;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.rafael.pizzeriadonmortandello.R;
import br.rafael.pizzeriadonmortandello.model.Pizza;

public class PizzaDataAdapter extends RecyclerView.Adapter<PizzaDataViewHolder> {

    private ArrayList<Pizza> pizzas;

    public PizzaDataAdapter(ArrayList<Pizza> pizzas){
        this.pizzas = pizzas;
    }

    @NonNull
    @Override
    public PizzaDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.pizza_menu_list,parent,false);
        PizzaDataViewHolder holder = new PizzaDataViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PizzaDataViewHolder holder, int position) {
        holder.bind(this.pizzas.get(position));
    }

    @Override
    public int getItemCount() {
        return this.pizzas.size();
    }

    @Override
    public int getItemViewType(int position){
        return super.getItemViewType(position);
    }
}
