package br.rafael.pizzeriadonmortandello.ui.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import br.rafael.pizzeriadonmortandello.R;
import br.rafael.pizzeriadonmortandello.model.Promocoes;

public class DialogPromo {

    public DialogPromo(Promocoes promo, Context context){
        AlertDialog.Builder build =  new AlertDialog.Builder(context);

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_promo,null);

        TextView promoNome = view.findViewById(R.id.dialog_promo_name);
        TextView promoDesc = view.findViewById(R.id.dialog_promo_safe);
        TextView promoDescricao = view.findViewById(R.id.dialog_promo_desc);
        Button btnPreco = view.findViewById(R.id.dialog_promo_btn);

        promoNome.setText(promo.getNome());
        promoDesc.setText(String.format("%.2f",promo.getDesconto())+"\b%");
        promoDescricao.setText(promo.mostraPromoçes());
        btnPreco.setText("R$\b\b"+promo.precoTotal());

        build.setTitle("Informações:");
        build.setView(view);
        AlertDialog delete = build.create();
        btnPreco.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                delete.dismiss();
            }
        });

        delete.show();
    }

}
