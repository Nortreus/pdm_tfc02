package br.rafael.pizzeriadonmortandello.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

import br.rafael.pizzeriadonmortandello.R;
import br.rafael.pizzeriadonmortandello.data.DAOPizzaSingleton;
import br.rafael.pizzeriadonmortandello.model.Pizza;

public class NewPizzaActivity extends AppCompatActivity {

    public static final String PIZZAS_KEY = "PizzaMenuActivity.PIZZAS";
    private static final ArrayList<Pizza> pizzas = new ArrayList<Pizza>();
    private EditText nome;
    private EditText preco;
    private EditText descricao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pizza);
        this.nome = findViewById(R.id.editTextNome);
        this.preco = findViewById(R.id.editTextPreco);
        this.descricao = findViewById(R.id.editTextDescricao);
    }

    public void onClickSalvar(View view){
        Intent passarPizzas = new Intent(this, MainActivity.class);
        if(!nome.getText().toString().isEmpty() && !preco.getText().toString().isEmpty() && !descricao.getText().toString().isEmpty()) {
            Pizza p = new Pizza(nome.getText().toString(), Double.parseDouble(preco.getText().toString()), descricao.getText().toString());
            DAOPizzaSingleton.getINSTANCE().addPizza(p);
            nome.setText("");
            preco.setText("");
            descricao.setText("");
        }
        startActivity(passarPizzas);



    }

}