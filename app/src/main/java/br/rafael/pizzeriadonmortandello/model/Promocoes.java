package br.rafael.pizzeriadonmortandello.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Promocoes implements Parcelable {

    private static long serial=1;

    private long id;
    private String nome;
    private Double desconto;
    private ArrayList<Pizza> pizzas;


    public Promocoes(String nome, Double desconto, ArrayList<Pizza> pizzas) {
        this.id = Promocoes.serial;
        this.nome = nome;
        this.desconto = desconto;
        this.pizzas = pizzas;
        Promocoes.serial++;
    }

    public String mostraPromoçes(){
        String promos = null;
        boolean first = true;
        for(Pizza p : this.pizzas){
            if(first == true){
                promos = p.getNome() + "\b\b\bR$\b"+String.format("%.2f",p.getPreco()*((100-this.desconto)/100))+"\n";
                first=false;
            }else{
                promos += p.getNome() + "\b\b\bR$\b"+String.format("%.2f",p.getPreco()*((100-this.desconto)/100))+"\n";
            }

        }
        return promos;
    }

    public String precoTotal(){
        Double preco = 0.0;
        for(Pizza p: this.pizzas){
            preco+=p.getPreco();
        }
        return String.format("%.2f",preco*((100-this.desconto)/100));
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public ArrayList<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(ArrayList<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    protected Promocoes(Parcel in) {
        this.id = in.readLong();
        this.nome = in.readString();
        if (in.readByte() == 0) {
            this.desconto = null;
        } else {
            this.desconto = in.readDouble();
        }
        this.pizzas = in.createTypedArrayList(Pizza.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.nome);
        if (this.desconto == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(this.desconto);
        }
        dest.writeTypedList(this.pizzas);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Promocoes> CREATOR = new Creator<Promocoes>() {
        @Override
        public Promocoes createFromParcel(Parcel in) {
            return new Promocoes(in);
        }

        @Override
        public Promocoes[] newArray(int size) {
            return new Promocoes[size];
        }
    };


}
