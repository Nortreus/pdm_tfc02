package br.rafael.pizzeriadonmortandello.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Pizza implements Parcelable {

    private static long serial=1;

    private long id;
    private String nome;
    private Double preco;
    private String descricao;

    public Pizza(String nome, double preco, String descicao) {
        this.id = Pizza.serial;
        this.nome = nome;
        this.preco = preco;
        this.descricao = descicao;
        Pizza.serial++;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNome() {
        return nome;
    }

    public Double getPreco() {
        return preco;

    }
    public Double getPrecoPequena(){
        return preco*0.25;
    }

    public Double getPrecoGrande(){
        return preco*1.25;
    }

    public String getDescricao() {
        return descricao;
    }

    //--------------------------------------------
    protected Pizza(Parcel in) {
        this.nome = in.readString();
        this.preco = in.readDouble();
        this.descricao = in.readString();
    }

    public static final Creator<Pizza> CREATOR = new Creator<Pizza>() {
        @Override
        public Pizza createFromParcel(Parcel in) {
            return new Pizza(in);
        }

        @Override
        public Pizza[] newArray(int size) {
            return new Pizza[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nome);
        dest.writeDouble(this.preco);
        dest.writeString(this.descricao);
    }
}
